-[中文](https://gitee.com/qiwushi/excelmerge/blob/master/README.md)



###用于Excel的GUI差异工具


https://gitee.com/qiwushi/excelmerge
![输入图片说明](Images/exceldiff.jpg)


##说明


ExcelMerge是Excel的图形显示工具。当前功能有Diff的显示，和部分合并功能
它也可以用作Git或Mercurial的diff工具。


##系统要求


-Windows 7或更高版本



##支持的文件类型

-.xlsx




##用法



###从快捷方式



！[](https://github.com/skanmera/ExcelMerge/blob/media/media/shortcut.png)



###从exproler上下文菜单



！[](https://github.com/skanmera/ExcelMerge/blob/media/media/context.png)



###从命令行



```

ExcelMerge。GUI差异[选项]

```



|选项|说明|类型|默认|

|------|-----------|----|-------|

|```-s`````--src路径``` |源文件路径|字符串|

|```-d`````--dst路径``` | Dest文件路径。|一串

|```-c````--外部cmd``` |它用于为不受支持的文件类型激活其他工具，并发生任何异常。|一串

|```-i`````--立即执行外部cmd``` |执行外部cmd而不出现错误对话框。|bool|false

|```-w`````--等待外部cmd``` |等待外部进程完成|bool|false

|```-v`````--验证扩展名``` |打开文件前验证扩展名|bool|false

|```-e`````--空文件名``` |空文件名|一串

|```-k `````--保留文件历史记录``` |不添加最近的文件|bool|false




##快捷键



|快捷键|说明|

|---|-----------|

|Ctrl+→|下一个修改的单元格|

|Ctrl+←|上一个修改的单元格|

|Ctrl+↓|下一个修改行|

|Ctrl+↑|上一个修改行|

|Ctrl+K|下一个添加的行|

|Ctrl+I|上一个添加的行|

|Ctrl+L|下一个删除的行|

|Ctrl+O|上一个删除的行|

|Ctrl+F|搜索单元格|

|F9|下一个匹配单元格|

|F8|上一个匹配单元格|

|Ctrl+C|将所选单元格复制为TSV|

|Ctrl+Shift+C|将所选单元格复制为CSV|

|Ctrl+D|显示（隐藏）控制台|

|Ctrl+B|将所选单元格差异输出为日志|




##将差异输出为日志



通过从上下文菜单中选择Ctrl+D或“输出日志”，可以将更改输出为日志。

格式可以从“差分提取设置”更改。






##已知问题



-<h4>如果有列删除或添加，它们可能不会显示在预期位置</h4>

如果当前显示的标头不是您所期望的，则可以通过指定适当的标头并提取差异来解决此问题。

按照以下步骤操作。

1.选择合适的标题单元格。

2.右键单击以显示上下文菜单。

3.选择“以此行为标题提取差异”




## LICENSE

#### MIT Licence

Copyright (c)2017 skanmera

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.