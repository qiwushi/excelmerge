﻿using System.Linq;
using System.Collections.Generic;
using System.Data;

namespace ExcelMerge
{
    public class ExcelRowDiff
    {
        public int Index { get; private set; }
        public SortedDictionary<int, ExcelCellDiff> Cells { get; private set; }
        public ExcelRowStatus Status { get; set; }
        public ExcelSaveStatus Saved { get; set; }

        public ExcelRowDiff(int index)
        {
            Index = index;
            Cells = new SortedDictionary<int, ExcelCellDiff>();
            Status = ExcelRowStatus.None;
            Saved = ExcelSaveStatus.None;
        }

        public ExcelCellDiff CreateCell(ExcelCell src, ExcelCell dst, int columnIndex, ExcelCellStatus status)
        {
            var cell = new ExcelCellDiff(columnIndex, Index, src, dst, status);
            Cells.Add(cell.ColumnIndex, cell);

            return cell;
        }

        public void ClearCellMergeStatus()
        {
            for (int i = 0; i < Cells.Count; i++)
            {
                Cells[i].MergeStatus = ExcelMergeStatus.None;
            }
        }

        public void SetCellMergeStatus(int column, ExcelMergeStatus mstat)
        {
            if(column >=0 && column < Cells.Count)
            {
                Cells[column].MergeStatus = mstat;
            }
        }

        public void UpdateCellMergeStatus()
        {
            for (int i = 0; i < Cells.Count; i++)
            {
                ExcelCellStatus stat = Cells[i].Status;
                if (stat != ExcelCellStatus.None)
                {
                    Cells[i].MergeStatus = ExcelMergeStatus.Merge;
                }
            }
        }

        public void UpdateRowStatus()
        {
            Status = GetStatus();
        }

        //检测状态
        public ExcelRowStatus GetStatus()
        {
            if (Saved == ExcelSaveStatus.Saved)
            {
                return ExcelRowStatus.None;
            }

            int totalnum = 0;
            int totalAddNum = 0;
            int totalRemoveNum = 0;
            int totalNoneNum = 0;
            bool isHaveNoneCell = false;
            for (int i = 0; i < Cells.Count; i++)
            {
                ExcelCellStatus stat = Cells[i].Status;
                if (stat == ExcelCellStatus.Modified)
                {
                    return ExcelRowStatus.Modified;
                }

                if (stat == ExcelCellStatus.None)
                {
                    isHaveNoneCell = true;
                }
                
                totalnum += (int)stat;
                totalNoneNum += (int)ExcelCellStatus.None;
                totalAddNum += (int)ExcelCellStatus.Added;
                totalRemoveNum += (int)ExcelCellStatus.Removed;
            }

            if (totalnum == totalNoneNum)
            {
                return ExcelRowStatus.None;
            }
            else
            {
                if (isHaveNoneCell)
                {
                    return ExcelRowStatus.Modified;
                }
                else
                {
                    if (totalnum == totalAddNum)
                    {
                        return ExcelRowStatus.Added;
                    }
                    else if (totalnum == totalRemoveNum)
                    {
                        return ExcelRowStatus.Removed;
                    }
                }
            }

            return ExcelRowStatus.Modified;
        }

        public bool RemoveCell(ExcelCellDiff rm)
        {
            return Cells.Remove(rm.ColumnIndex);
        }

        public bool IsModified()
        {
            return Cells.Any(c => c.Value.Status != ExcelCellStatus.None);
        }

        public bool IsAdded()
        {
            return Cells.All(c => c.Value.Status == ExcelCellStatus.Added);
        }

        public bool IsRemoved()
        {
            return Cells.All(c => c.Value.Status == ExcelCellStatus.Removed);
        }

        public int ModifiedCellCount
        {
            get { return Cells.Count(c => c.Value.Status != ExcelCellStatus.None); }
        }


        // TODO: Add row status field and implemnt UpdateStaus method.
    }
}
