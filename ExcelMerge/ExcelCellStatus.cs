﻿namespace ExcelMerge
{
    /*
     * 这个状态是左边相对于于右边的，以左边为基准，Added就是相对于左边增加，Removed就是相对于左边删除
     */
    public enum ExcelCellStatus
    {
        None = 0,       //一样的
        Modified,   //都有的，有修改
        Added,      //左边没有，右边有
        Removed,    //左边有，右边没有
    }

    public enum ExcelMergeStatus
    {
        None = 0,   //无，不存盘
        Merge,      //有，存盘
    }

    /*
        * 这个状态是左边相对于于右边的，以左边为基准，Added就是相对于左边增加，Removed就是相对于左边删除
     */
    public enum ExcelRowStatus
    {
        None = 0,       //一样的
        Modified,   //都有此行，有修改
        Added,      //左边没有此行，右边有此行
        Removed,    //左边有此行，右边没有此行
    }

    public enum ExcelSaveStatus
    {
        None = 0,   
        Saved,   //已存盘
    }
}
