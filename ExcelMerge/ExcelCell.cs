﻿using NPOI.SS.UserModel;

namespace ExcelMerge
{
    public class ExcelCell
    {
        public string Value { get; set; }
        public int OriginalColumnIndex { get; set; }
        public int OriginalRowIndex { get; set; }

        public ICell Icell { get; set; }

        public ExcelCell(string value, int originalColumnIndex, int originalRowIndex, ICell cell =null)
        {
            Value = value;
            OriginalColumnIndex = originalColumnIndex;
            OriginalRowIndex = originalRowIndex;
            Icell = cell;
        }
    }
}
